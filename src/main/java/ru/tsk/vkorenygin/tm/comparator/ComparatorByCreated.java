package ru.tsk.vkorenygin.tm.comparator;

import ru.tsk.vkorenygin.tm.api.entity.IHasCreated;

import java.util.Comparator;

public class ComparatorByCreated implements Comparator<IHasCreated> {

    public static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    private ComparatorByCreated() {
    }

    @Override
    public int compare(IHasCreated o1, IHasCreated o2) {
        return o1.getCreateDate().compareTo(o2.getCreateDate());
    }

}
