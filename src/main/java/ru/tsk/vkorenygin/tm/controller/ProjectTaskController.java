package ru.tsk.vkorenygin.tm.controller;

import ru.tsk.vkorenygin.tm.api.controller.IProjectTaskController;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.api.service.IProjectTaskService;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectService projectService,
                                 ITaskService taskService,
                                 IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null)
            throw new TaskNotFoundException();
        Task taskTiedToProject = projectTaskService.bindTaskToProject(projectId, taskId);
        if (taskTiedToProject == null)
            throw new TaskNotFoundException();
        else System.out.println("[Task tied to project]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null)
            throw new TaskNotFoundException();
        Task taskUntiedToProject = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (taskUntiedToProject == null)
            System.out.println("[Task not untied to project]");
        else
            System.out.println("[Task untied to project]");
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null)
            throw new ProjectNotFoundException();
        List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        if (tasks == null) System.out.println("[No tasks found in the project]");
        else {
            int index = 1;
            for (Task task : tasks) System.out.println(index++ + ". " + task.toString());
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null)
            throw new ProjectNotFoundException();
        final Project removedProject = projectTaskService.removeProjectById(projectId);
        if (removedProject == null)
            throw new ProjectNotFoundException();
        else System.out.println("[Project removed]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("Enter index:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectTaskService.removeProjectByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        else System.out.println("[Project removed]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
        else System.out.println("[Project removed]");
    }

}
