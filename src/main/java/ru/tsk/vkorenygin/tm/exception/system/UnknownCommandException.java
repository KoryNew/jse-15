package ru.tsk.vkorenygin.tm.exception.system;

import ru.tsk.vkorenygin.tm.constant.TerminalConst;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Error! Command not found. Use ```" +
                TerminalConst.HELP +
                "``` to display the list of available commands");
    }

}
